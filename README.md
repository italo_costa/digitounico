**PASSO A PASSO**

Os passos seguintes vão lhe permitir executar os teste unitários, compilar, empacotar e executar o projeto

---

## Compilar e Executar os testes

Com o Maven configurado no seu ambiente, navegue até o diretório raiz do seu projeto com o seu terminal de preferência:

1. mvn clean package

---

## Executando a aplicação

Agora vamos testar a aplicação... Para executá-la basta executar o comando abaixo no diretório raiz:

1. mvn spring-boot:run

---

## Swagger

Para visualizar o arquivo do Swagger basta acessar[http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).

## Collection Postman com testes

A Collection está na raiz do projeto com o nome 'postman_collection.json'
