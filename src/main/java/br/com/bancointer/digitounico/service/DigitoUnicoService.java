package br.com.bancointer.digitounico.service;

import java.util.Collections;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import br.com.bancointer.digitounico.util.Util;

@Service
public class DigitoUnicoService {
	
	
	public Integer calculaDigitoUnico(String base, Integer repeticoes) {
		String baseNova = Collections.nCopies(repeticoes, base)
				.stream()
				.collect(Collectors.joining());
		
		Integer digitoUnico = verificaCache(baseNova);
		if(digitoUnico != null)
			return digitoUnico;
		digitoUnico = calculaDigitoUnicoInterno(baseNova);
		adicionaCache(baseNova, digitoUnico);
		return digitoUnico;
	}
	

	private Integer calculaDigitoUnicoInterno(String valor) {
		if(valor.length()>1) {
			Integer digito = valor.chars()
					.mapToObj(c-> Character.getNumericValue(c))
					.mapToInt(o -> o.intValue())
					.sum();
			return calculaDigitoUnicoInterno(digito.toString());
		}else {
			return Integer.parseInt(valor);
		}
	}
	
	private Integer verificaCache(String base) {
		return Util.MEU_CACHE.get(base);
	}
	private void adicionaCache(String base, Integer digitoUnico) {
		Util.MEU_CACHE.put(base, digitoUnico);
	}
	

}
