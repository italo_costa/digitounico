package br.com.bancointer.digitounico.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import br.com.bancointer.digitounico.exception.UsuarioNaoEncontradoException;
import br.com.bancointer.digitounico.model.DigitoUnico;
import br.com.bancointer.digitounico.model.Usuario;
import br.com.bancointer.digitounico.model.dto.UsuarioDTO;
import br.com.bancointer.digitounico.repository.UsuarioRepository;
import br.com.bancointer.digitounico.util.Util;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public List<Usuario> listaUsuarios() {
		List<Usuario> usuarios = usuarioRepository.findAll();
		usuarios
			.forEach(u -> {
				if(!StringUtils.isEmpty(u.getChavePublica())) {
					aplicaCriptografia(u);
				}
			});
		return usuarios;
	}
	
	public Usuario recuperaUsuario(Integer id) throws UsuarioNaoEncontradoException {
		Optional<Usuario> optional = usuarioRepository.findById(id);
		if(optional.isPresent()) {
			if(!StringUtils.isEmpty(optional.get().getChavePublica())) {
				aplicaCriptografia(optional.get());
			}
			return optional.get();
		}
		throw new UsuarioNaoEncontradoException();
	}
	
	public Usuario criaUsuario(UsuarioDTO usuarioDTO) {
		Usuario usuario = new Usuario(usuarioDTO.getNome(),usuarioDTO.getEmail());
		return usuarioRepository.save(usuario);
	}
	
	public void inserirChavePublica(Integer id, String chavePublica) throws UsuarioNaoEncontradoException {
		Usuario usuario = recuperaUsuario(id);
		usuario.setChavePublica(chavePublica);
		usuarioRepository.save(usuario);
	}
	
	public Usuario atualizaUsuario(Integer id, UsuarioDTO usuarioDTO) throws UsuarioNaoEncontradoException {
		Usuario usuario = recuperaUsuario(id);
		usuario.setNome(usuarioDTO.getNome());
		usuario.setEmail(usuarioDTO.getEmail());
		return usuarioRepository.save(usuario);
	}
	
	public void removeUsuario(Integer id) {
		usuarioRepository.deleteById(id);
	}
	
	public void adicionaDigitoUnico(Integer id, String base, Integer qtdRepeticoes, Integer digitoUnico) throws UsuarioNaoEncontradoException {
		DigitoUnico digitoUnicoObj = new DigitoUnico(base, qtdRepeticoes, digitoUnico);
		Usuario usuario = recuperaUsuario(id);
		usuario.getDigitosCalculado().add(digitoUnicoObj);
		usuarioRepository.save(usuario);
	}
	
	private Usuario aplicaCriptografia(Usuario usuario) {
		try {
			usuario.setNome(Util.encrypt(usuario.getNome(), usuario.getChavePublica()));
			usuario.setEmail(Util.encrypt(usuario.getEmail(), usuario.getChavePublica()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return usuario;
	}
	
	
}
