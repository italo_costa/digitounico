package br.com.bancointer.digitounico.util;

import java.util.LinkedHashMap;
import java.util.Map;

public class MeuCache extends LinkedHashMap<String, Integer>{

	private static final long serialVersionUID = 1L;
	
	private static final int MAX_ENTRIES = 10;
	
	
	 
    public MeuCache() {
		super(MAX_ENTRIES, .75f, true);
	}



	@Override
    protected boolean removeEldestEntry(Map.Entry<String,Integer> eldest) {
        return size() > MAX_ENTRIES;
    }

}
