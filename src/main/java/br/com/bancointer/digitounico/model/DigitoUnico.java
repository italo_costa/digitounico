package br.com.bancointer.digitounico.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor
public class DigitoUnico {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String base;
	private Integer qtdRepeticoes;
	private Integer digitoUnico;
	
	public DigitoUnico(String base, Integer qtdRepeticoes, Integer digitoUnico) {
		this.base = base;
		this.qtdRepeticoes = qtdRepeticoes;
		this.digitoUnico = digitoUnico;
	}
	
	
		
}
