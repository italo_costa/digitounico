package br.com.bancointer.digitounico.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter @Setter @NoArgsConstructor 
public class Usuario implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String nome;
	private String email;
	@Lob
	private String chavePublica;
	
	@OneToMany(cascade = CascadeType.ALL)
	private List<DigitoUnico> digitosCalculado;

	public Usuario(String nome, String email) {
		this.nome = nome;
		this.email = email;
	}
	
	

}
