package br.com.bancointer.digitounico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import br.com.bancointer.digitounico.util.MeuCache;
import br.com.bancointer.digitounico.util.Util;

@SpringBootApplication
public class DigitounicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigitounicoApplication.class, args);
		Util.MEU_CACHE = new MeuCache();
	}

}
