package br.com.bancointer.digitounico.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.digitounico.exception.UsuarioNaoEncontradoException;
import br.com.bancointer.digitounico.model.DigitoUnico;
import br.com.bancointer.digitounico.model.Usuario;
import br.com.bancointer.digitounico.service.DigitoUnicoService;
import br.com.bancointer.digitounico.service.UsuarioService;
import io.swagger.annotations.Api;

@Api(value="API REST Dígito Único")
@RestController
@RequestMapping("/digitos-unico")
public class DigitoUnicoResource {
	
	@Autowired
	private UsuarioService usuarioService;
	@Autowired
	private DigitoUnicoService digitoUnicoService;
	
	@GetMapping(value = "/{idUsuario}" ,produces = "application/json")
	public ResponseEntity<List<DigitoUnico>> recuperaDigitosPorUsuario(@PathVariable(value = "idUsuario") Integer idUsuario){
		
		try {
			Usuario usuario = usuarioService.recuperaUsuario(idUsuario);
			return ResponseEntity.ok(usuario.getDigitosCalculado());
		} catch (UsuarioNaoEncontradoException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(value = "/calcula")
	public ResponseEntity<Integer> calculaDitoUnico(
			@RequestParam(value = "base") String base,
			@RequestParam(value = "repeticoes")Integer repeticoes,
			@RequestParam(value = "idUsuario", required = false)Integer idUsuario){
		try {
			Integer digitoUnico = digitoUnicoService.calculaDigitoUnico(base, repeticoes);
			if(idUsuario != null)
				usuarioService.adicionaDigitoUnico(idUsuario, base, repeticoes, digitoUnico);
			return ResponseEntity.ok(digitoUnico);
		} catch (UsuarioNaoEncontradoException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
}
