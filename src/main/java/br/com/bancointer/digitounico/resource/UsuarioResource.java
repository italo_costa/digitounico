package br.com.bancointer.digitounico.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.bancointer.digitounico.exception.UsuarioNaoEncontradoException;
import br.com.bancointer.digitounico.model.Usuario;
import br.com.bancointer.digitounico.model.dto.UsuarioDTO;
import br.com.bancointer.digitounico.service.UsuarioService;

@RestController
@RequestMapping("/usuarios")
@SuppressWarnings("rawtypes")
public class UsuarioResource {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping(produces="application/json")
	public ResponseEntity<List<Usuario>> listaUsuarios(){
		
		return ResponseEntity.ok(usuarioService.listaUsuarios());
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")
	public ResponseEntity<Usuario> recuperaUsuario(@PathVariable(value="id") Integer id) {
		
		try {
			Usuario usuario = usuarioService.recuperaUsuario(id);
			return ResponseEntity.ok(usuario);
		} catch (UsuarioNaoEncontradoException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(produces = "application/json")
	public ResponseEntity<Usuario> criaUsuario(@RequestBody UsuarioDTO usuarioDTO){
		return new ResponseEntity<Usuario>(usuarioService.criaUsuario(usuarioDTO),HttpStatus.CREATED);
		
	}
	
	@PatchMapping(value="/{id}" , produces = "application/json")
	public ResponseEntity atualizaUsuario(@PathVariable(value="id") Integer id,@RequestBody UsuarioDTO usuarioDTO) {
		try {
			usuarioService.atualizaUsuario(id,usuarioDTO);
			return ResponseEntity.ok().build();
		} catch (UsuarioNaoEncontradoException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	@PatchMapping(value="/{id}/aplica-criptografia" , produces = "application/json")
	public ResponseEntity aplicaCriptografia(
			@PathVariable(value="id") Integer id,
			@RequestParam(value="chavePublica") String chavePublica) {
		try {
			usuarioService.inserirChavePublica(id,chavePublica);
			return ResponseEntity.ok().build();
		} catch (UsuarioNaoEncontradoException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity removeUsuario(@PathVariable(value = "id") Integer id) {
		usuarioService.removeUsuario(id);
		return ResponseEntity.ok().build();
	}
	
	
	
}
