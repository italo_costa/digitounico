package br.com.bancointer.digitounico;

import java.io.IOException;
import java.security.GeneralSecurityException;

import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.bancointer.digitounico.exception.UsuarioNaoEncontradoException;
import br.com.bancointer.digitounico.model.Usuario;
import br.com.bancointer.digitounico.model.dto.UsuarioDTO;
import br.com.bancointer.digitounico.service.UsuarioService;
import br.com.bancointer.digitounico.util.Util;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioServiceTest {
	
	@Autowired
	private UsuarioService usuarioService;
	@Rule
	public ExpectedException thrown = ExpectedException.none();
	
	@Test
	public void criarEValidarNaBase() {
		UsuarioDTO usuarioDTO = new UsuarioDTO("italo costa", "italo@email.com");
		Usuario usuario = usuarioService.criaUsuario(usuarioDTO);
		
		Assertions.assertThat(usuario.getId()).isNotNull();
		Assertions.assertThat(usuario.getNome()).isEqualTo("italo costa");
		Assertions.assertThat(usuario.getEmail()).isEqualTo("italo@email.com");
		
	}
	
	@Test
	public void removerEValidarNaBase() throws UsuarioNaoEncontradoException {
		thrown.expect(UsuarioNaoEncontradoException.class);
		UsuarioDTO usuarioDTO = new UsuarioDTO("italo costa", "italo@email.com");
		Usuario usuario = usuarioService.criaUsuario(usuarioDTO);
		usuarioService.removeUsuario(usuario.getId());
		
		Assertions.assertThat(usuarioService.recuperaUsuario(usuario.getId())).isNull();
	}
	
	@Test
	public void AtualizarEValidarNaBase() throws UsuarioNaoEncontradoException {
		
		UsuarioDTO usuarioDTO = new UsuarioDTO("italo costa", "italo@email.com");
		Usuario usuario = usuarioService.criaUsuario(usuarioDTO);
		usuarioDTO.setNome("Italo R Costa");
		usuarioService.atualizaUsuario(usuario.getId(), usuarioDTO);
		usuario = usuarioService.recuperaUsuario(usuario.getId());
		
		Assertions.assertThat(usuario.getNome()).isEqualTo("Italo R Costa");
	}
	
	@Test
	public void inserirChavePublicaEValidarCriptografia() throws UsuarioNaoEncontradoException, IOException, GeneralSecurityException {
		String chavePrivada = "MIIEowIBAAKCAQEAibEq+b1TV0ObfutN6vT+nMDdlkSBhl1B7JNbCVj1gq26q6tS\r\n" + 
				"Ai6Pl+H+RxTxVnlqg3CjkaCYURiwjnyutoqFquTLslm5D8iEA96WiqRCsEynGdoP\r\n" + 
				"a9TV17VAqd+A2s0wgFxmE8dw3ZqdDGI0AjIcNJnG/KJbKAlZ8krYyP/KqVrSPPCr\r\n" + 
				"Pc8roPUXVK3UzOpr/OkF3ogjCJTYrSMGrvQn+exmkvkvHyqTUFg/m1BC8L9lLiPD\r\n" + 
				"wG3udNjTpIwQm2rAT8tV8d7q5OadFeRfPBjsVDyLlqM5bbtjvHn7xrGbnV5vqe7Y\r\n" + 
				"NDm8cz9ubbAkcMNvaSXxi9jFGCXPnNi5ePeD9wIDAQABAoIBAA4dSSh8jdR+K6xg\r\n" + 
				"9oXFQ2xSIOTMdPKPELjtigF1SwVwRZhfYXKzzyhklCo5LVwN6is7GqlM6xA1FbQl\r\n" + 
				"d8tie70+4tqPc/b4OQAkiw3/NI7Z7K8ruTT2hk0sWUqQjKbhLb2auyP/58Pvm3Wv\r\n" + 
				"g2FOPbHuqYUwJ2P9QWki3wILpnyhmiUxFByk2f3lfcJnoJX2J12GJXWZGi7QXEGw\r\n" + 
				"//7wi62Z4qfV622vpmte4+ElBznPI5BH3RXdthoi5ACrxMgkg8avO74s37xei1lv\r\n" + 
				"/NgJclObCSTclAW8Pel8cQt/Vg9DyJ42jhCFwxH1tvMPjOy03It/eX5SfQsLBT23\r\n" + 
				"tjCiwhECgYEAyED6bkq/0SQRht6iETmOq0JegP9AuOFR30EBah/dcEe36FBiGqWB\r\n" + 
				"/FqSL8FvPj7c0C27kfx2lhTaDB2MRQUyvYumx+kb4FbWxiq+2CLsUuE1x56W6770\r\n" + 
				"D9biu9rICrqLhsSvp2ixB3LBDODMWAhGqgbaM5IxpM37xxoY3Mq7cQMCgYEAsAW/\r\n" + 
				"KZoVHxbb9ML36SoUcWcuuI3wKLCANXn0RtJJwcArL3diStSKLTaoSoitaG0M+v81\r\n" + 
				"nO52eFBo8GTwzVfTt6aImz3b494TNCC/7MQIdeTWQ+9LTEyHoIBdw8XKkGfl5Cuf\r\n" + 
				"g3H9eyqvlIHlZkXRjHBlAdAKiEMmcL4ByjlJnP0CgYEAmI2XuknLkwHNMFeOU1zr\r\n" + 
				"qN/oqpDJt44/Frqo3zXjNdQM8De8ZNxNB/ffge30VxEAmr8cM3yGwpKkAbLI1xO8\r\n" + 
				"sdUfew/EosTE3TZXKMSCECltFONlybYwq5hiJgXnevTdsg9IWVpR8oCjPLJMOYwk\r\n" + 
				"eQa7V6o5k3kwI19oLVtsLA0CgYBDsxIg+AgjmvEiJFKhNPQD4cGQbdbub2QALVWM\r\n" + 
				"X5jKkhSJ/wqo6THzzc/QSt0XCsoxRzLhlr2t7I9P1iLfd2YQflaQDqyh6WnNZMMs\r\n" + 
				"VwFCBq96FXEKeWy4iGaHw2zzOWKr7TM+pPrvAi3hmkwWl8fOkfPnTJKgceM4/XtG\r\n" + 
				"wd3UKQKBgH5+C/enCE+lhKb3UDVtXEHJsKi+zXTX/ZjErQeM81FEs01rTrEjbaNn\r\n" + 
				"WpODNiQqsu9/tbcjroVlj4K8OgACyFuVCdZ3HYbSTgDsX/XrD9a2UatnRlfJ+jJG\r\n" + 
				"vkYdsXYwHUmI4WDBMumrMTljPljECIHRF0bPOW78ppIFf64Sn/ih";
		
		UsuarioDTO usuarioDTO = new UsuarioDTO("italo costa", "italo@email.com");
		Usuario usuario = usuarioService.criaUsuario(usuarioDTO);
		
		usuarioService.inserirChavePublica(usuario.getId(), "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAibEq+b1TV0ObfutN6vT+\r\n" + 
				"nMDdlkSBhl1B7JNbCVj1gq26q6tSAi6Pl+H+RxTxVnlqg3CjkaCYURiwjnyutoqF\r\n" + 
				"quTLslm5D8iEA96WiqRCsEynGdoPa9TV17VAqd+A2s0wgFxmE8dw3ZqdDGI0AjIc\r\n" + 
				"NJnG/KJbKAlZ8krYyP/KqVrSPPCrPc8roPUXVK3UzOpr/OkF3ogjCJTYrSMGrvQn\r\n" + 
				"+exmkvkvHyqTUFg/m1BC8L9lLiPDwG3udNjTpIwQm2rAT8tV8d7q5OadFeRfPBjs\r\n" + 
				"VDyLlqM5bbtjvHn7xrGbnV5vqe7YNDm8cz9ubbAkcMNvaSXxi9jFGCXPnNi5ePeD\r\n" + 
				"9wIDAQAB");
		
		Usuario usuarioCriptografado = usuarioService.recuperaUsuario(usuario.getId());
		
		Assertions.assertThat(Util.decrypt(usuarioCriptografado.getNome(),chavePrivada)).isEqualTo("italo costa");
		Assertions.assertThat(Util.decrypt(usuarioCriptografado.getEmail(),chavePrivada)).isEqualTo("italo@email.com");
		
	}
	
}
